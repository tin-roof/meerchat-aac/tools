/**
@TODO: 
- [ ] Clear the registration form after a successful registration
- [ ] Disable the registration fields
- [ ] Style the dialog
- [ ] Get and update all the images
*/

let Application = async (settings = null) => {
  const LSUAT = "_aat";
  const URL = "https://aac.tools";
  const AURL = "https://api.meerchat.app";
  const defaultToken = // this token is used when the user is not logged in
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQG1lZXJjaGF0LmFwcCIsIm5hbWUiOiIiLCJpc3MiOiJtZWVyY2hhdCIsImlhdCI6MTY5OTcxMjgwOCwianRpIjoiZGIyZTZlZmQtM2ZhZi00OTdkLWFiNGItYzE4Y2RkNTk4MTZmIn0.stTQ_sKziVuZyHskumZQlOeLnZhBMbU1mGtcAr3Eel4";

  // get the passwordless client
  const pClient = new Passwordless.Client({
    apiKey: "meerchat:public:1e7ec201ef4348ff893c02bf05d02b31",
  });

  let App = {
    Settings: null,

    api: {
      board: null,
      image: null,
    },

    elements: {
      userDetails: null,
      boardSearchBar: null,
    },

    Boards: {
      currentQuery: null,
      handler: null,
      searchPagination: null,
      searchResults: [],

      // buttonHandler handles button clicking and processing
      ButtonHandler: async function ButtonHandler(event) {
        const buttonID = event.target.closest(".drop-zone").id;

        let button = this.boardDetails.buttons.find(
          (btn) => btn.buttonID == buttonID
        );

        // skip button processing if it was recently hit or spammed
        if (
          this.lastButtonHit &&
          this.lastButtonHit.id === button.buttonID &&
          Date.now() - this.lastButtonHit.time < buttonCoolDown &&
          this.lastButtonHit.count < buttonCoolDownHits
        ) {
          this.lastButtonHit.count++;
          this.lastButtonHit.time = Date.now();
          return;
        }

        // set the last button hit
        this.lastButtonHit = {
          count: 1,
          id: button.buttonID,
          time: Date.now(),
        };

        // track the button hit
        this.api.analytics.Track(
          this.api.analytics.NewButtonEvent(
            App.User.profile.id,
            Pages.AAC.BoardHistory.Current(),
            button
          )
        );

        // add the button to the speech to the phrase
        if (button.addToPhrase) {
          this.Phrase.Add(button);
        }

        // say the button speech now
        if (button.sayNow) {
          if (button.speech && button.speech !== "") {
            this.Phrase.Read(button.speech);
          } else {
            this.Phrase.Read(button.label);
          }
        }

        // open a link to external/internal pages
        if (button.openLink && button.link && button.link !== "") {
          const link = button.link.split("::");
          if (link.length !== 3) {
            // @TODO: invalid link log it
            return;
          }

          switch (link[0]) {
            case "internal":
              const board = await this.api.board.Load(link[1]);
              this.LoadBoard(board);
              this.BoardHistory.Push(link[1]); // add the new board to the history
              break;
            case "external":
              window.open(link[1], "_blank");
              break;
            default:
            // @TODO: invalid link log it
          }
        }
      },

      Init: function Init() {
        // setup the API
        this.api.board = new window.MeerchatAPI.board();
        this.api.image = new window.MeerchatAPI.image();

        // setup any elements needed for the page
        this.elements.boardSearchBar = document.querySelector(
          "#boards .search .bar search-bar"
        );
        this.elements.boardSearchBar.setOnSubmit(this.Boards.Search.bind(this));

        // setup the toolbar buttons
        this.elements.printButton = document.querySelector(
          "#boards .toolbar #printBoard"
        );
        this.elements.printButton.addEventListener("click", (e) => {
          // check to make sure the button is not disabled
          if (App.Utility.IsButtonDisabled(e)) {
            return;
          }

          window.print();
        });
        this.elements.newButton = document.querySelector(
          "#boards .toolbar #createBoard"
        );
        this.elements.newButton.addEventListener("click", (e) => {
          // check to make sure the button is not disabled
          if (App.Utility.IsButtonDisabled(e)) {
            return;
          }

          const modal = document.querySelector("#newBoardMessage");
          modal.classList.remove("hidden");
        });
        this.elements.shareButton = document.querySelector(
          "#boards .toolbar #shareBoard"
        );
        this.elements.shareButton.addEventListener("click", (e) => {
          // check to make sure the button is not disabled
          if (App.Utility.IsButtonDisabled(e)) {
            return;
          }

          const modal = document.querySelector("#shareBoardMessage");

          // update the QR code
          const qr = modal.querySelector("#shareCode qr-code");
          qr.setData(window.location.href, { size: 125 });

          // update the share link
          modal.querySelector("#shareLink").value = window.location.href;

          modal.classList.remove("hidden");
        });
        this.elements.searchButton = document.querySelector(
          "#boards .toolbar #searchBoards"
        );
        this.elements.searchButton.addEventListener("click", (e) => {
          // check to make sure the button is not disabled
          if (App.Utility.IsButtonDisabled(e)) {
            return;
          }

          // get the view
          const view = document.querySelector("#boards .view");
          view.innerHTML = "";
          view.classList.add("hidden");

          // hide the search
          const search = document.querySelector("#boards .search");
          search.classList.remove("hidden");

          // disabled the extra buttons here
          this.elements.printButton.classList.add("disabled");
          this.elements.shareButton.classList.add("disabled");
          this.elements.searchButton.classList.add("disabled");
          this.elements.newButton.classList.remove("disabled");

          // reset the url to remove the board ID
          window.history.pushState({}, "", "/boards");
        });

        // setup the board handler
        this.Boards.handler = new window.AAC.board({
          apis: this.api,
        });

        // check to see if there is a board id in the URL and load the board if thee is
        const urlParams = new URLSearchParams(window.location.search);
        const boardID = urlParams.get("board");
        if (boardID && boardID !== "") {
          this.Boards.View(boardID);
        }
      },

      // Render renders a board for the search results
      Render: async function Render(board = null) {
        const grid = window.AAC.calculateRatio(board.grid);

        const result = document.createElement("div");
        result.classList.add("result");
        result.addEventListener("click", () => this.View(board.id));
        result.dataset.boardId = board.id;

        const preview = document.createElement("div");
        preview.classList.add("preview");

        // render the board preview and put in the preview
        preview.appendChild(
          await this.handler.render(board, null, {
            zoom: 0.18,
            width: "calc(100% - 5rem)",
          })
        );
        result.appendChild(preview);

        const details = document.createElement("div");
        details.classList.add("details");
        details.innerHTML = `
          <div class="title" title="${board.title}">${board.title}</div>
          <div class="description">
            <i class="far fa-info-circle"></i>
            <div class="extra">
              <div class="grid-size">${grid.columns}x${grid.rows}</div>
              <div class="desc" title="">${board.description}</div>
            </div>
          </div>
        `;
        result.appendChild(details);

        return result;
      },

      // Search handles searching for boards in the system
      Search: async function Search(query = "") {
        let queryChanged = false;
        let getTotalPages = false;
        let currentPage = this.Boards.searchPagination
          ? this.Boards.searchPagination.CurrentPage()
          : 1;
        this.Boards.seearchResults = null;

        // reset query information based on whats happing with the search bar
        if (this.Boards.currentQuery !== query) {
          this.Boards.currentQuery = query;
          queryChanged = true;
          getTotalPages = true;
        }

        // check to see if the user is logged in and display a message if they are not
        userID = this.User.GetUserID();
        if (!userID) {
          userID = "db2e6efd-3faf-497d-ab4b-c18cdd59816f"; // @NOTE: this is the main meerchat user
        }

        // use the API to make the request
        // @TODO: implement some kind of caching for search so paging doesn't always hit the db
        const response = await this.api.board.Search(
          this.Boards.currentQuery.toLowerCase(),
          userID,
          userID,
          currentPage,
          getTotalPages,
          true
        );

        // loop over all the boards and render them into the page
        if (response && response.boards && response.boards.length > 0) {
          this.Boards.searchResults = response.boards;

          const results = document.querySelector("#boards .results");
          results.innerHTML = "";
          results.classList.remove("no-results");

          for (const board of response.boards) {
            results.appendChild(await this.Boards.Render(board));
          }
        } else {
          const results = document.querySelector("#boards .results");
          results.innerHTML = "";
          results.classList.add("no-results");
          return;
        }

        // reset pagination if the search query changed
        if (queryChanged) {
          // setup the pagination if its not already setup
          if (!this.Boards.searchPagination) {
            this.Boards.searchPagination = new Pagination({
              currentPage: 1,
              totalPages: response.totalPages,
              container: document.querySelector("#boards .search #pagination"),
              onChange: () => this.Boards.Search.bind(this),
            });
          }

          this.Boards.searchPagination.SetPages(
            currentPage,
            response.totalPages
          );
        }
      },

      // View handles loading the full view of the selected board. This is where the editing/printing/etc will happen
      View: async function View(boardId = "") {
        let board = this.searchResults.find((b) => b.id === boardId);

        // search for the board id if it wasn't found in the search results
        if (!board) {
          board = await App.api.board.Load(boardId);
          if (!board) {
            console.error("Board not found.");
            return;
          }
        }

        // set the board ID into the url
        window.history.pushState({}, "", `?board=${board.id}`);

        // get the view
        const view = document.querySelector("#boards .view");
        view.innerHTML = "";
        view.appendChild(
          // @TODO: build the handler for the buttons to atleast say the word when clicked
          // await this.handler.render(board, this.ButtonHandler.bind(this))
          await this.handler.render(board)
        );
        view.classList.remove("hidden");

        // hide the search
        const search = document.querySelector("#boards .search");
        search.classList.add("hidden");

        // enable the extra buttons here
        App.elements.printButton.classList.remove("disabled");
        App.elements.shareButton.classList.remove("disabled");
        App.elements.searchButton.classList.remove("disabled");
        App.elements.newButton.classList.add("disabled");
      },
    },

    // User contains all things pertaining to a user such as authorization/login/registration
    User: {
      details: null,
      imageBucket: null,

      Auth: {
        // Check checks to see if an auth token is set already or not
        Check: async function Auth() {
          const token = window.localStorage.getItem(LSUAT);
          if (!token || token === defaultToken) {
            // set the default token here so that the user can still use features that require the API while logged out
            window.localStorage.setItem(LSUAT, defaultToken);
            return false;
          }

          // validate the token that was found
          App.elements.userDetails.setLoading(true);
          const authorized = await App.User.Auth.ValidateToken(token);
          if (authorized) {
            // set the user details into the component
            App.elements.userDetails.setUserDetails(
              App.User.details.name ?? App.User.details.email,
              App.User.GetProfileImageURL(35)
            );
            App.elements.userDetails.setLoading(false);
            return true;
          }

          App.elements.userDetails.setLoading(false);
          return false;
        },

        // Login handles logging in the user
        Login: async function Login(email = "") {
          App.elements.userDetails.setLoading(true);

          // make sure the email field has some thing in it
          // @TODO: make sure the email is an email..
          if (!email || email === "" || email.length < 3) {
            App.Utility.Dialog("Please enter your email address.", "warn");
            App.elements.userDetails.setLoading(false);
            return false;
          }

          // sign the user in
          token = await pClient.signinWithAlias(email);

          // make the request to verify the user and return the auth token
          const resp = await window.MeerchatAPI.fetch(
            "POST",
            `${AURL}/auth/passwordless`,
            { email: email, token: token.token },
            null,
            false
          );

          // set the auth token into local storage and redirect to the app
          if (resp.status === "success") {
            window.localStorage.setItem(LSUAT, resp.token);
            await App.User.Auth.ValidateToken(resp.token);

            // set the user details into the component
            App.elements.userDetails.setUserDetails(
              App.User.details.name ?? App.User.details.email,
              App.User.GetProfileImageURL(35)
            );

            App.elements.userDetails.setLoading(false);
            return true;
          }

          App.Utility.Dialog(
            "Something went wrong while getting your account setup, please try again.",
            "error"
          );
          App.elements.userDetails.setLoading(false);
          return false;
        },

        // Logout handles logging the user out
        Logout: async function Logout() {
          // remove the auth token from local storage
          window.localStorage.removeItem(LSUAT);

          // redirect the user to the login page
          window.location.href = `${URL}`;
        },

        // Register handles reading the token from the url sent in the verficaiton email and registers the user
        Register: async (token = "") => {
          if (!token) {
            App.Utility.Dialog("Invalid verification token.", "error");
            return;
          }

          App.elements.userDetails.setLoading(true);

          // make request to the system register the user
          const resp = await window.MeerchatAPI.fetch(
            "PUT",
            `${AURL}/auth/passwordless`,
            { token: token },
            null,
            false
          );

          // register the user because an account matching email was not found
          if (resp.token) {
            try {
              await pClient.register(resp.token);
              App.Utility.Dialog(`Whoohoo! You can login now.`);
              App.elements.userDetails.setLoading(false);
              return true;
            } catch (e) {
              App.Utility.Dialog(
                `Error registering your account: ${e}`,
                "error"
              );
              App.elements.userDetails.setLoading(false);
            }
          }

          App.Utility.Dialog("Error registering your account.", "error");
        },

        // ValidateToken checks if an auth token is valid and sets the user details
        ValidateToken: async function ValidateToken(token = "") {
          // check the users auth token
          let response = await window.MeerchatAPI.fetch(
            "GET",
            `${AURL}/user/profile`,
            null,
            null,
            false
          );

          // authorization passed
          if (response.status == "success") {
            // set users details
            App.User.details = response.profile;
            return true;
          }

          // remove the auth token from local storage
          window.localStorage.removeItem(LSUAT);
          return false;
        },

        // Verifyemail handles sending the email verification email
        VerifyEmail: async (email = "", name = "") => {
          // make sure the email field has some thing in it
          if (!email || email === "" || email.length < 3) {
            App.Utility.Dialog("Please enter your email address.", "warn");
            return false;
          }

          // make sure the name field has some thing in it
          if (!name || name === "" || name.length < 1) {
            App.Utility.Dialog("Please enter your name.", "warn");
            return false;
          }

          App.elements.userDetails.setLoading(true);

          // make request to the system register the user
          const resp = await window.MeerchatAPI.fetch(
            "POST",
            `${AURL}/auth/email`,
            {
              action: "register",
              email: email,
              site: "aac.tools",
              name: name,
            },
            null,
            false
          );

          // register the user because an account matching email was not found
          if (resp.status === "success") {
            App.Utility.Dialog(
              `We sent you an email to ${email} to verify your account.`
            );

            App.elements.userDetails.setLoading(false);
            return true;
          }

          App.Utility.Dialog(
            "Error sending email verification email.",
            "error"
          );
          App.elements.userDetails.setLoading(false);
          return false;
        },
      },

      // gets the image bucket name
      GetImageBucket: function GetImageBucket(force = false) {
        // build the bucket string if its not already built
        if (force || this.imageBucket === null || this.imageBucket === "") {
          this.imageBucket = this.details.id.replaceAll("-", "");
        }

        return this.imageBucket;
      },

      // builds the url for the profile image
      GetProfileImageURL: (size = 50) => {
        if (
          App.User.details &&
          App.User.details.image &&
          App.User.details.image !== ""
        ) {
          // @TODO: we have uploaded images set the url heres
        }

        return `https://www.gravatar.com/avatar/${App.User.GetImageBucket()}?s=${size}&d=identicon`;
      },

      // get the users id
      GetUserID: () => {
        if (App.User.details && App.User.details.id) {
          return App.User.details.id;
        }

        return "";
      },
    },

    Utility: {
      CopyToClipboard: (field = null) => {
        if (!field) {
          return;
        }

        // select the text field
        field.select();
        field.setSelectionRange(0, 99999); // For mobile devices

        // Copy the text inside the text field
        navigator.clipboard.writeText(field.value);

        // Alert the copied text
        App.Utility.Dialog(`Copied (${field.value}) to clipboard!`);
      },

      // add a status message to the page
      Dialog: (message = null, type = "success", time = 6000) => {
        let dialog = document.querySelector("#dialog");

        // switch dialog type
        switch (type) {
          case "error":
            dialog.classList.add("error");
            dialog.classList.remove("success");
            dialog.classList.remove("warn");
            break;
          case "warn":
            dialog.classList.add("warn");
            dialog.classList.remove("success");
            dialog.classList.remove("error");
            break;
          case "success":
          default:
            dialog.classList.add("success");
            dialog.classList.remove("error");
            dialog.classList.remove("warn");
        }

        // update message and show the dialog
        dialog.innerHTML = message;
        dialog.classList.remove("hidden");

        // remove dialog after 6 seconds
        setTimeout(() => {
          dialog.classList.add("hidden");
        }, time);
      },

      // check to see if a button that was clicked is disabled or not
      IsButtonDisabled: (e) => {
        return (
          e.target.classList.contains("disabled") ||
          e.target.parentElement.classList.contains("disabled")
        );
      },

      // log data to the console when debug mode is active
      Log: (data) => {
        if (App.Settings.debug) {
          console.log(data);
        }
      },

      Wait: (delay, ...args) =>
        new Promise((resolve) => setTimeout(resolve, delay, ...args)),
    },
  };

  // initialize the App
  return (async () => {
    // add settings to the app
    App.Settings = settings;
    settings = null;

    // wait a few seconds to load resources
    await App.Utility.Wait(1000);

    // get the user-details component and set the data
    App.elements.userDetails = document.querySelector("user-details");
    App.elements.userDetails.setOnLogin(App.User.Auth.Login.bind(this));
    App.elements.userDetails.setOnLogout(App.User.Auth.Logout.bind(this));
    App.elements.userDetails.setOnVerify(App.User.Auth.VerifyEmail.bind(this));

    // check to see if the user is already logged in
    const loggedIn = await App.User.Auth.Check();

    // check to see if there is a registration token in the url and process it if there is
    if (!loggedIn) {
      const searchParams = new URLSearchParams(window.location.search);

      if (searchParams.has("registration_token")) {
        const token = searchParams.get("registration_token");
        if (token) {
          await App.User.Auth.Register(token);
        }
      }
    }

    return {
      boards: {
        init: App.Boards.Init.bind(App),
      },
      utility: {
        copyToClipboard: App.Utility.CopyToClipboard.bind(App),
      },
    };
  })();
};
