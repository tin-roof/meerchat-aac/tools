"use strict";

customElements.define(
  "site-footer",
  class extends HTMLElement {
    constructor() {
      super();

      // attaches shadow tree and returns shadow root reference
      const shadow = this.attachShadow({ mode: "open" });
      this.shadowRoot.innerHTML = `
        <link rel="stylesheet" href="./js/components/website/footer/style.min.css" />
      `;

      // create the component
      this.element = document.createElement("div");
      const element = this.element;

      const currentYear = new Date().getFullYear();

      element.innerHTML = `
        <div class="footer">
          <div class="links">
            <div class="connection">
              <div class="title">Stay Connected</div>
              <div class="subtitle">Join us on:</div>
              <div class="social">
                <a href="https://join.slack.com/t/meerchat-aac/shared_invite/zt-22uhgs1ck-deDeWnaTV0JxxFl5ngKuYw" target="_blank" rel="noopener noreferrer">
                  <svg height="32px" version="1.1" viewBox="0 0 512 512" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:serif="http://www.serif.com/" xmlns:xlink="http://www.w3.org/1999/xlink"><g><g><path d="M107.57,323.544c0,29.603 -24.182,53.785 -53.785,53.785c-29.602,0 -53.785,-24.182 -53.785,-53.785c0,-29.602 24.183,-53.785 53.785,-53.785l53.785,0l0,53.785Z" style="fill:#e01e5a;fill-rule:nonzero;"/><path d="M134.671,323.544c0,-29.602 24.183,-53.785 53.785,-53.785c29.603,0 53.785,24.183 53.785,53.785l0,134.671c0,29.603 -24.182,53.785 -53.785,53.785c-29.602,0 -53.785,-24.182 -53.785,-53.785l0,-134.671Z" style="fill:#e01e5a;fill-rule:nonzero;"/></g><g><path d="M188.456,107.57c-29.602,0 -53.785,-24.182 -53.785,-53.785c0,-29.602 24.183,-53.785 53.785,-53.785c29.603,0 53.785,24.183 53.785,53.785l0,53.785l-53.785,0Z" style="fill:#36c5f0;fill-rule:nonzero;"/><path d="M188.456,134.671c29.603,0 53.785,24.183 53.785,53.785c0,29.603 -24.182,53.785 -53.785,53.785l-134.671,0c-29.602,0 -53.785,-24.182 -53.785,-53.785c0,-29.602 24.183,-53.785 53.785,-53.785l134.671,0Z" style="fill:#36c5f0;fill-rule:nonzero;"/></g><g><path d="M404.43,188.456c0,-29.602 24.183,-53.785 53.785,-53.785c29.603,0 53.785,24.183 53.785,53.785c0,29.603 -24.182,53.785 -53.785,53.785l-53.785,0l0,-53.785Z" style="fill:#2eb67d;fill-rule:nonzero;"/><path d="M377.329,188.456c0,29.603 -24.182,53.785 -53.785,53.785c-29.602,0 -53.785,-24.182 -53.785,-53.785l0,-134.671c0,-29.602 24.183,-53.785 53.785,-53.785c29.603,0 53.785,24.183 53.785,53.785l0,134.671Z" style="fill:#2eb67d;fill-rule:nonzero;"/></g><g><path d="M323.544,404.43c29.603,0 53.785,24.183 53.785,53.785c0,29.603 -24.182,53.785 -53.785,53.785c-29.602,0 -53.785,-24.182 -53.785,-53.785l0,-53.785l53.785,0Z" style="fill:#ecb22e;fill-rule:nonzero;"/><path d="M323.544,377.329c-29.602,0 -53.785,-24.182 -53.785,-53.785c0,-29.602 24.183,-53.785 53.785,-53.785l134.671,0c29.603,0 53.785,24.183 53.785,53.785c0,29.603 -24.182,53.785 -53.785,53.785l-134.671,0Z" style="fill:#ecb22e;fill-rule:nonzero;"/></g></g></svg>
                </a>
                <a href="https://www.linkedin.com/company/meerchat-aac/" target="_blank" rel="noopener noreferrer">
                  <svg enable-background="new 0 0 32 32" height="32px" id="Layer_1" version="1.0" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M32,30c0,1.104-0.896,2-2,2H2c-1.104,0-2-0.896-2-2V2c0-1.104,0.896-2,2-2h28c1.104,0,2,0.896,2,2V30z" fill="#007BB5"/><g><rect fill="#FFFFFF" height="14" width="4" x="7" y="11"/><path d="M20.499,11c-2.791,0-3.271,1.018-3.499,2v-2h-4v14h4v-8c0-1.297,0.703-2,2-2c1.266,0,2,0.688,2,2v8h4v-7    C25,14,24.479,11,20.499,11z" fill="#FFFFFF"/><circle cx="9" cy="8" fill="#FFFFFF" r="2"/></g></g><g/><g/><g/><g/><g/><g/></svg>
                </a>
              </div>
            </div>
            <div class="mission">
              <div class="title">About</div>
              <div>
                AAC tools is a collection of tools brought to you by Meerchat AAC and our community 
                to help make AAC more accessible and affordable for everyone.
              </div>
            </div>
            <div class="navigate">
              <div class="title">Navigate</div>
              <div class="pages">
                <a href="https://aac.tools/boards">Boards</a>
                <a href="https://aac.tools/faq">FAQs</a>
                <a href="https://meerchat.app">Meerchat AAC</a>
              </div>
            </div>
          </div>
          <div class="copy">
            &copy;
            ${currentYear}
            <a href="https://aac.tools">AAC tools</a> by <a href="https://meerchat.app">Meerchat</a>
          </div>
          <div class="logo"></div>
        </div>
      `;

      // add the element to the shadow DOM
      shadow.appendChild(element);
    }
  }
);
